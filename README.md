# Google Summer of Code 2023 Final Report
**Name**: Eveline Anderson
<br>
**Mentor**: Sybren Stüvel
<br>
**Project**: Flamenco Improvements
<br>
**Organization**: Blender
<br>
**Description**: Around a year ago, Flamenco—a rendering-farm Blender addon—was released to improve the workflow for animation artists across the globe. It is extremely successful, but still has many key functionalities that can greatly improve it. The goal of this project was to build upon the foundation of Flamenco and add features that could refine Flamenco and push it to its next version 3.3


## What Work Was Done
In this project, the majority of time I spent was on the client side. This was due to most of my tasks dealt with frontend challenges, rather than the heavy backend database. Here are the bullet points of the work that was performed during my time at GSoC:
* Creation of Worker Status Tracker: [Pull Request #104217](https://projects.blender.org/studio/flamenco/pulls/104217)
* Renaming of Worker Cluster to Tags for Clearer Naming: [Pull Request #104223](https://projects.blender.org/studio/flamenco/pulls/104223)
* Implemenation of SocketIO Refetch After Disconnect: [Pull Request #104235](https://projects.blender.org/studio/flamenco/pulls/104235)
* Creation of Web Interface for Tags: [Pull Request #104244](https://projects.blender.org/studio/flamenco/pulls/104244)

## Future Work
Although my current proposal is far different from the tasks that I completed, I did get to reach my goal of helping Flamenco push to version 3.3. I wanted to do far more with this project in the beginning, but I am happy with the progress that I have made. If time permits me, I would love to do interfacing work for Flamenco. Of course, realistically, I cannot guarantee this will be the case as I intend to have a fulltime role elsewhere that may take up most of my mental energy. Flamenco in a whole is an amazing project and has an amazing community.

## Acknowledgements
* I really thank my mentor for all his patience. Those weekly meetings were a life savor and I was able to get so much done.
* I thank the community as well, I had a couple of users help me debug issues that I could not solve myself. This really helped me progress further in the project.
* Flamenco Documentation was very well written! I was extremely impressed and it assisted me alot with different questions I had on the project's structure.